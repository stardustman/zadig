# 调研


## zadig 使用用户

开发、测试、运维。环境隔离。默认有 dev，qa 环境。
Zadig 项目支持但不限于以下技术场景：

基础设施：Kubernetes、主机。
环境创建方式：新建环境、托管现有环境（加载现有集群资源）。
服务部署方式：K8s YAML 部署、Helm Chart 部署。

## 项目

## 构建

### 方式
1. zadig 本身提供构建
2. Jenkins 集成

![avatar](./image/zadig.build.png)


### 项目 > 高级配置

1. 可以自定义交付物名称中，可以自定义镜像规则。解决不同环境下打包的问题。
```bash
镜像和 TAR 包规则可以通过变量和常量组装生成：
{{.TIMESTAMP}} 时间戳
{{.TASK_ID}} 工作流任务 ID
{{.REPO_BRANCH}} 代码分支名称
{{.REPO_PR}} 代码 PR ID
{{.REPO_TAG}} 代码 TAG
{{.REPO_COMMIT_ID}} 代码 Commit ID
{{.PROJECT}} 项目名称
{{.SERVICE}} 服务名称
{{.ENV_NAME}} 环境名称 
注意：常量字符只能是大小写字母、数字、中划线、下划线和点，即 [a-zA-Z0-9_.-]，首个字符不能是 . 或 -。不能超过 127 个字符
```


## 安装应用，用来构建，测试等

> 需要的依赖，制作后可以复用。

### nodejs
> https://docs.koderover.com/zadig/settings/app/#node

Bin Path $HOME/node/bin
安装包地址 https://nodejs.org/dist/v16.13.0/node-v16.13.0-linux-x64.tar.xz
安装脚本：
```bash
mkdir -p $HOME/node
tar -C $HOME/node -xzf ${FILEPATH} --strip-components=1
npm config --global set registry https://registry.npm.taobao.org

```

## 服务部署方式

## 流水线

### 触发
push , pull_request

### 结果通知